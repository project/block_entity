<?php

/**
 * Implements hook_entity_info().
 */
function block_entity_entity_info() {
  // Get a list of all modules that provide blocks to set our bundles.
  $block_modules = block_entity_get_modules();
  $bundles = array();
  foreach ($block_modules as $block_module) {
    $module_info = system_get_info('module', $block_module);
    if (!empty($module_info['name'])) {
      $bundles[$block_module] = array(
        'label' => $module_info['name'],
      );
    }
  }

  // Create the "block_entity" entity type.
  // @todo Should we call this entity type "block" instead of "block_entity"?
  // @todo Consider making this fieldable?  Would need an admin UI.
  // @todo If making this fieldable, will also need to be translatable.
  $entity_info['block_entity'] = array(
    'label' => t('Block Entity'),
    'label callback' => 'block_entity_label_callback',
    'entity class' => 'BlockEntity',
    'controller class' => 'BlockEntityController',
    'base table' => 'block_entity',
    'uri callback' => 'block_entity_uri', // Required to avoid Field API errors.
    'fieldable' => FALSE, // We are not storing additional data for blocks.
    'entity keys' => array(
      'id' => 'id',
      'bundle' => 'module',
    ),
    'load hook' => 'block_entity_load_entity',
    'static cache' => TRUE,
    'module' => 'block_entity',
    'bundles' => $bundles, // These are modules
    'view modes' => array(
      'default' => array(
        'label' => t('Default'),
        'custom settings' => FALSE,
      ),
      'full' => array(
        'label' => t('Full'),
        'custom settings' => TRUE,
      ),
      'label' => array(
        'label' => t('Label'),
        'custom settings' => TRUE,
      ),
      'nothing' => array(
        'label' => t('Nothing'),
        'custom settings' => FALSE,
      ),
    ),
    'views controller class' => 'EntityDefaultViewsController',
    'access callback' => 'block_entity_access_callback', // Required to avoid Field API errors.
  );

  return $entity_info;
}

/**
 * Callback for hook_entity_info().
 *
 * @return bool
 */
function block_entity_access_callback() {
  return TRUE; // Defer to block permissions.
//  return user_access('administer blocks') && !user_is_anonymous();
}

/**
 * Callback for hook_entity_info().
 *
 * @see https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/callback_entity_info_uri/7
 *
 * @param \BlockEntity $entity The entity being referenced.
 * @return array
 */
function block_entity_uri($entity) {
//  return array('path' => '');
  return array();
}


/**
 * Implements hook_flush_caches().
 */
function block_entity_flush_caches() {
  block_entity_rebuild_table();
  return;
}

/**
 * This is the primary function that populates the entity's base table.
 *
 * @throws \Exception
 */
function block_entity_rebuild_table() {
  // Get the difference between what blocks exist and what is stored in the db.
  $block_info = block_entity_get_blocks();
  $block_mds = array_keys($block_info);
  $records = block_entity_get_records();
  $record_mds = array();
  foreach ($records as $record) {
    $record_mds[] = $record->module . ':' . $record->delta;
  }
  $delete_mds = array_diff($record_mds, $block_mds);
  $insert_mds = array_diff($block_mds, $record_mds);

  // Delete records that aren't needed.
  foreach ($delete_mds as $md) {
    list($module, $delta) = explode(':', $md);
    db_delete('block_entity')
      ->condition('module', $module)
      ->condition('delta', $delta)
      ->execute();
  }
  // @todo Delete associated fields if that functionality is added. Invoke a hook?

  // Insert records that are needed.
  $values = array();
  foreach ($insert_mds as $md) {
    list($module, $delta) = explode(':', $md);
    $values[] = array(
      'module' => $module,
      'delta' => $delta,
    );
  }
  if (!empty($values)) {
    $query = db_insert('block_entity')->fields(array('module', 'delta'));
    foreach ($values as $record) {
      $query->values($record);
    }
    $query->execute();
  }
}

/**
 * Get database records from the entity's base table.
 *
 * @param array $ids (Optional) A list of IDs to lookup.
 * @return array A list of block data.
 */
function block_entity_get_records($ids = array()) {
  $query = db_select('block_entity', 'be')
    ->addTag('block_entity')
    ->fields('be', array('id', 'module', 'delta'));
  if (!empty($ids)) {
    // Load all entities if no ids were specified.
    $query->condition('id', $ids, 'IN');
  }
  $result = $query->execute();
  $output = array();
  foreach ($result as $record) {
    $output[$record->id] = $record;
  }
  return $output;
}

/**
 * Get a list of the modules that provide blocks.
 *
 * @return array A list of modules that provide blocks.
 */
function block_entity_get_modules() {
  $modules = module_implements('block_info');
  asort($modules);
  return $modules;
}

/**
 * Get a list of blocks.
 *
 * @param string $module (Optional) A module to reference.
 * @return array A list of hook_block_info() data.
 */
function block_entity_get_blocks($module = '') {
  $modules = array();
  if (empty($module)) {
    $modules = block_entity_get_modules();
  }
  else {
    $modules[] = $module;
  }

  $block_info = array();
  foreach ($modules as $module) {
    $module_blocks = module_invoke($module, 'block_info');
    if (!empty($module_blocks)) {
      foreach ($module_blocks as $delta => $block) {
        $block = (object) $block;
        $block->module = $module;
        $block->delta = $delta;
        $block->bid = "{$block->module}:{$block->delta}";
        $block_info[$block->bid] = $block;
      }
    }
  }

  ksort($block_info);
  return $block_info;
}

/**
 * Get a list of block names.
 *
 * @param string $module (Optional) A module to reference.
 * @return array A list of block names.
 */
function block_entity_get_block_names($module = '') {
  $block_names = array();
  $block_info = block_entity_get_blocks($module);

  foreach ($block_info as $bid=>$block) {
    if (property_exists($block, 'info')) {
      $block_names[$bid] = $block->info;
    }
  }
  natcasesort($block_names);

  return $block_names;
}

/**
 * Callback for loading an entity.
 */
function block_entity_load_entity($id, $reset = FALSE) {
  $block = block_entity_load_multiple_entities(array($id), array(), $reset);
  return reset($block);
}

/**
 * Callback for loading multiple entities.
 */
function block_entity_load_multiple_entities(array $ids, $conditions = array(), $reset = FALSE) {
  return entity_load('block_entity', $ids, $conditions, $reset);
}

/**
 * Render the full block, similar to block.module.
 *
 * @param object $block.
 * @return string The rendered block.
 */
function block_entity_render($entity) {
  $block = block_load($entity->module, $entity->delta);
  $render_array = _block_get_renderable_array(_block_render_blocks(array($block)));
  return render($render_array);
}

/**
 * Get the administrative name for the block to use as the entity label.
 *
 * @param object $block The block entity.
 * @param string $entity_type
 * @return string The name of the block.
 */
function block_entity_label_callback($block, $entity_type) {
  $block_name = t('Untitled block');
  if (property_exists($block, 'module') && property_exists($block, 'delta')) {
    $bid = $block->module . ':' . $block->delta;
    $block_names = block_entity_get_block_names($block->module);
    $block_name = $block_names[$bid];
  }
  return $block_name;
}

class BlockEntity extends Entity {
  /**
   * Override the buildContent method.
   */
  public function label() {
    return t(block_entity_label_callback($this));
  }
}

class BlockEntityController extends EntityAPIController {
  /**
   * Override the buildContent method.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);
    $block_content = '';

    // Either render the full block or just the title.
    if($this->checkBlockAccess($entity)) {
      switch ($view_mode) {
        case 'nothing':
          break;

        case 'label':
          $block_content = t(block_entity_label_callback($entity, 'block_entity'));
          break;

        case 'full':
        case 'default':
        default:
          $block_content = block_entity_render($entity);
          break;

      }
    }

    // Add the rendered block to the build array.
    if ($view_mode != 'nothing') {
      $build['block_entity'] = array(
        '#type' => 'markup',
        '#markup' => $block_content,
      );
    }

    return $build;
  }

  /**
   * Override the view method.
   */
  public function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    // For Field API and entity_prepare_view, the entities have to be keyed by
    // (numeric) id.
    $entities = entity_key_array_by_property($entities, $this->idKey);
    if (!empty($this->entityInfo['fieldable'])) {
      field_attach_prepare_view($this->entityType, $entities, $view_mode);
    }
    entity_prepare_view($this->entityType, $entities);
    $langcode = isset($langcode) ? $langcode : $GLOBALS['language_content']->language;

    $view = array();
    foreach ($entities as $entity) {
      $build = entity_build_content($this->entityType, $entity, $view_mode, $langcode);
      $build += array(
        // If the entity type provides an implementation, use this instead the
        // generic one.
        // @see template_preprocess_entity()
//        '#theme' => 'entity',
        // By removing this ^^, it prevents the entity label and typical entity HTML wrapper from being added.
        // @todo Consider using a different theme function for our entity instead.
        '#entity_type' => $this->entityType,
        '#entity' => $entity,
        '#view_mode' => $view_mode,
        '#language' => $langcode,
        '#page' => $page,
      );
      // Allow modules to modify the structured entity.
      drupal_alter(array($this->entityType . '_view', 'entity_view'), $build, $this->entityType);
      $key = isset($entity->{$this->idKey}) ? $entity->{$this->idKey} : NULL;
      $view[$this->entityType][$key] = $build;
    }
//    dpm($view, 'BlockEntityController:view():$view');
    return $view;
  }

  /**
   * Check block access by their module
   *
   * @param $entity
   * @return bool|int
   */
  public function checkBlockAccess($entity)
  {
    $block_type = $entity->module;

    switch($block_type){
      case 'views':
        $access = (!$this->checkBlockViewsAccess($entity) || !$this->checkBlockBlockAccess($entity)) ? 0 : 1;
        break;
      default:
        $access = $this->checkBlockBlockAccess($entity);
        break;
    }

    return $access;
  }

  /**
   * Load the view and check the access
   *
   * @param $entity
   * @return bool
   */
  public function checkBlockViewsAccess($entity)
  {
    $view_info = explode('-', $entity->delta);
    $view = views_get_view($view_info[0]);

    return !is_null($view) ? $view->access($view_info[1]) : 1;
  }

  /**
   * Check block module access
   *
   * @param $entity
   * @return int
   */
  public function checkBlockBlockAccess($entity)
  {
    $block_object = array(block_load($entity->module, $entity->delta));
    $block_object[0]->status = 1;

    block_block_list_alter($block_object);
    return !empty($block_object) ? 1 : 0;
  }

  /**
   * Override the resetCache method.
   */
  public function resetCache(array $ids = NULL) {
    parent::resetCache($ids);
    block_entity_rebuild_table();
  }

  /**
   * Override the load method.
   */
  public function load($ids = array(), $conditions = array()) {
    $output = parent::load($ids, $conditions);

    // @todo The rest of this function needs caching.

    if (empty($output)) {
      $output = block_entity_get_records($ids);
    }

    $blocks = array();
    foreach ($output as $id=>$record) {
      $blocks[$id] = block_load($record->module, $record->delta);
    }

    return $blocks;
  }

  /**
   * Override the create method.
   */
  public function create(array $values = array()) {
    return; // Do nothing.
  }

  /**
   * Override the save method.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    return; // Do nothing.
  }

  /**
   * Override the delete method.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    return; // Do nothing.
  }
}
